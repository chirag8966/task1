import { Injectable } from '@angular/core';
import dummyCustomers from './MOCK_DATA.json';

@Injectable({
  providedIn: 'root'
})

export class CustomerService {
  public dummyCustomers = dummyCustomers;

  constructor() { }

  getCustomers() {
    return dummyCustomers;
  }
}
