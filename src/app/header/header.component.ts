import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  loadedFeature = 'null';

  onSelect(feature: string) {
    this.loadedFeature = feature ;
  }

  constructor() { }

  ngOnInit() {
  }
}
