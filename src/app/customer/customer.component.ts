import { Component, OnInit, Inject } from '@angular/core';
import { CustomerService } from '../shared/services/customer/customer.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  totalRecords = 20;
  customers: any;
  jobNoSort: boolean;
  dateSort: boolean;
  custNameSort: boolean;


  constructor(private service: CustomerService) { }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.customers  = this.service.getCustomers();
  }

}
